import { TypeOrmModuleOptions} from '@nestjs/typeorm'

export const config: TypeOrmModuleOptions = {
    type: 'postgres',
    username: 'postgres',
    password: '123',
    port: 5432,
    host: 'localhost',
    database: 'runninghilldb',
    synchronize: true,
    entities: ["dist/**/*.entity{.ts,.js}"]
}