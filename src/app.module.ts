import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { config } from './orm.config'
import { WordModule } from './word/word.module';
import { SentencesModule } from './sentences/sentences.module';

@Module({
  imports: [TypeOrmModule.forRoot(config), WordModule, SentencesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
