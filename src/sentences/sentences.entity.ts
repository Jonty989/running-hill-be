import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('setences')
export class SentenceEntity {
    @PrimaryGeneratedColumn() id;

    @Column('text') sentence: string;
}