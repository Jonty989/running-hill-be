import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createSentenceDto } from './dto/create-sentence.dto';
import { SentenceEntity } from './sentences.entity';

@Injectable()
export class SentencesService {

    constructor(@InjectRepository(SentenceEntity) private _sentenceRepository: Repository<SentenceEntity>){}

    async createSentence(data: createSentenceDto){
        const newWord = await this._sentenceRepository.create(data)
        await this._sentenceRepository.save(newWord);
        return newWord
    }
}
