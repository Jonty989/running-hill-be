import { Body, Controller, Post } from '@nestjs/common';
import { createSentenceDto } from './dto/create-sentence.dto';
import { SentencesService } from './sentences.service';

@Controller('sentences')
export class SentencesController {

    constructor(private _sentencesService: SentencesService){

    }

    @Post()
    createWord(@Body() data: createSentenceDto){
        return this._sentencesService.createSentence(data)
    }
}
