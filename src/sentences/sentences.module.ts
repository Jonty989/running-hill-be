import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SentencesController } from './sentences.controller';
import { SentenceEntity } from './sentences.entity';
import { SentencesService } from './sentences.service';

@Module({
  imports: [TypeOrmModule.forFeature([SentenceEntity])],
  controllers: [SentencesController],
  providers: [SentencesService]
})
export class SentencesModule {}
