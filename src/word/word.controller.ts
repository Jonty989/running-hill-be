import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { createWordDto } from './dto/create-word.dto';
import { getWordDto } from './dto/get-word.dto';
import { WordService } from './word.service';

@Controller('word')
export class WordController {

    constructor(private _wordService: WordService){

    }

    @Post()
    createWord(@Body() data: createWordDto){
        return this._wordService.createWord(data)
    }
    
    @Get(':type')
    getWordsbyType(@Param() type: getWordDto){
        return this._wordService.getWordsbyType(type)
    }

    @Delete(':id')
    deleteWord(@Param() id: number){
        return this._wordService.deleteWord(id)
    }

}
