import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('words')
export class WordEntity {
    @PrimaryGeneratedColumn() id;

    @Column('text') word: string;

    @Column('text') wordType: string;
}