export interface createWordDto {
    word: string;
    wordType: string;
}