import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { createWordDto } from './dto/create-word.dto';
import { getWordDto } from './dto/get-word.dto';
import { WordEntity } from './word.entity';

@Injectable()
export class WordService {

    constructor(@InjectRepository(WordEntity) private _wordRepository: Repository<WordEntity>){}


    async getWordsbyType(type: getWordDto){
        return await this._wordRepository.find({where : { wordType: type.type }})
    }

    async createWord(data: createWordDto){
        const newWord = await this._wordRepository.create(data)
        await this._wordRepository.save(newWord);
        return newWord
    }

    async deleteWord(id: number){
        console.log(id)
        await this._wordRepository.delete(id)
        return {deleted: "success"}
    }
}
